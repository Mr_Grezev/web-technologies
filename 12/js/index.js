﻿
$(function () { 
$('#signupModal').on('shown.bs.modal', function (event) {
    var form = event.relatedTarget.closet("#signup-form");
    var oname = /*document.getElementById*/$(form).find("signup-oname").val();
    var pname = /*document.getElementById*/$(form).find("signup-pname").val();
    var pasw = /*document.getElementById*/$(form).find("signup-pasw").val();
    var confpasw = /*document.getElementById*/$(form).find("signup-confpasw").val();
    var breed = /*document.getElementById*/$(form).find("signup-breed").val();


    var modal = /*document.querySelector*/$(this);
    modal.find('.modal-title').text('New account for ' + pname);
    modal.find('.modal-body #modal-oname').val(oname);
    modal.find('.modal-body #modal-pname').val(pname);
    modal.find('.modal-body #modal-pasw').val(pasw);
    modal.find('.modal-body #modal-confpasw').val(confpasw);
    modal.find('.modal-body #modal-breed').val(breed);
})

});
