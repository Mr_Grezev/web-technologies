﻿window.onload = function() {
    var header = document.getElementsByTagName('header')[0];
    var buttons = header.childNodes;
    /*buttons[1].onclick = function() {
        normalize();
        var cats = document.getElementById('cats');
        var dogs = document.querySelectorAll('#dogs')[0];
        if (cats.style.width == '5%') {
            dogs.style.animation = 's5-95 0.5s forwards';
            cats.style.animation = 's95-5 0.5s forwards';
        }
        else {
            dogs.style.animation = 's5-50 0.5s forwards';
            cats.style.animation = 's95-50 0.5s forwards';
        }
        
        dogs.style.width = '95%';
        cats.style.width = '5%';
        dogs.className = 's5';
        cats.className = '';
    }
    buttons[2].onclick = function() {
        var cats = document.getElementById('cats');
        var dogs = document.getElementById('dogs');
        var cost = cats.style.width;
        cats.style.width = dogs.style.width;
        dogs.style.width = cost;
        dogs.style.animation = 's50 0.5s forwards';
        cats.style.animation = 's50 0.5s forwards';
        dogs.onload = function() {
            document.getElementById('cats').style.width = '50%';
            document.getElementById('dogs').style.width = '50%';
        }
        normalize();
    }
    buttons[3].onclick = function() {
        normalize();
        var dogs = document.getElementById('dogs');
        var cats = document.querySelectorAll('#cats')[0];
        if (dogs.style.width == '5%') {
            dogs.style.animation = 's95-5 0.5s forwards';
            cats.style.animation = 's5-95 0.5s forwards';
        }
        else {
            dogs.style.animation = 's95-50 0.5s forwards';
            cats.style.animation = 's5-50 0.5s forwards';
        }
        dogs.style.width = '5%';
        cats.style.width = '95%';
        cats.className = 's5';
        dogs.className = '';
    }*/
    buttons[1].onclick = function() {
        normalize();
        var cats = document.getElementById('cats');
        var dogs = document.querySelectorAll('#dogs')[0];
        cats.classList.add('s95');
        dogs.classList.add('s05');
    }
    buttons[2].onclick = function() {
        normalize();
        var cats = document.getElementById('cats');
        var dogs = document.getElementById('dogs');
        cats.classList.add('s50');
        dogs.classList.add('s50');
    }
    buttons[3].onclick = function() {
        normalize();
        var dogs = document.getElementById('dogs');
        var cats = document.querySelectorAll('#cats')[0];
        cats.classList.add('s05');
        dogs.classList.add('s95');
    }
}

function normalize() {
    var cats = document.getElementById('cats');
    var dogs = document.getElementById('dogs');
    dogs.className = '';
    cats.className = '';
}