﻿document.addEventListener('DOMContentLoaded', () => {

    var kit = document.getElementById('kit');
    var all_img = document.getElementById('all_img');
    var gav = document.getElementById('gav');

    var cat = document.getElementById('cat');
    var dog = document.getElementById('dog');

    var c1 = document.getElementById('c1');
    var c2 = document.getElementById('c2');
    var c3 = document.getElementById('c3');

    var ci1 = document.getElementById('ci1');
    var ci2 = document.getElementById('ci2');
    var ci3 = document.getElementById('ci3');

    var d1 = document.getElementById('d1');
    var d2 = document.getElementById('d2');
    var d3 = document.getElementById('d3');

    var di1 = document.getElementById('di1');
    var di2 = document.getElementById('di2');
    var di3 = document.getElementById('di3');

    all_img.addEventListener("click", view_all, false);
    kit.addEventListener("click", view_kit, false);
    gav.addEventListener("click", view_dog, false);

    function view_all() {
        cat.style.width = "50%";
        cat.style.minHeight = "100%";

        dog.style.width = "50%";
        dog.style.minHeight = "100%";

        c1.style.display = 'block';
        c2.style.display = 'block';
        c3.style.display = 'block';
        ci1.style.display = 'block';
        ci2.style.display = 'block';
        ci3.style.display = 'block';

        d1.style.display = 'block';
        d2.style.display = 'block';
        d3.style.display = 'block';
        di1.style.display = 'block';
        di2.style.display = 'block';
        di3.style.display = 'block';

        ci1.style.width = "500px";
        ci1.style.height = "500px";
        ci2.style.width = "500px";
        ci2.style.height = "400px";
        ci3.style.width = "500px";
        ci3.style.height = "500px";

        di1.style.width = "500px";
        di1.style.height = "500px";
        di2.style.width = "500px";
        di2.style.height = "500px";
        di3.style.width = "500px";
        di3.style.height = "500px";

    }

    function view_kit() {

        cat.style.width = "1000px";
        cat.style.minHeight = "100%";

        dog.style.width = "200px";
        dog.style.minHeight = "100%";

        c1.style.display = 'block';
        c2.style.display = 'block';
        c3.style.display = 'block';
        ci1.style.display = 'block';
        ci2.style.display = 'block';
        ci3.style.display = 'block';
        ci1.style.width = "900px";
        ci1.style.height = "900px";
        ci2.style.width = "900px";
        ci2.style.height = "800px";
        ci3.style.width = "900px";
        ci3.style.height = "900px";

        d1.style.display = 'none';
        d2.style.display = 'none';
        d3.style.display = 'none';
        di1.style.display = 'none';
        di2.style.display = 'none';
        di3.style.display = 'none';


    }

    function view_dog() {

        dog.style.width = "1000px";
        dog.style.minHeight = "100%";

        cat.style.width = "200px";
        cat.style.minHeight = "100%";

        d1.style.display = 'block';
        d2.style.display = 'block';
        d3.style.display = 'block';
        di1.style.display = 'block';
        di2.style.display = 'block';
        di3.style.display = 'block';
        di1.style.width = "900px";
        di1.style.height = "900px";
        di2.style.width = "900px";
        di2.style.height = "900px";
        di3.style.width = "900px";
        di3.style.height = "900px";

        c1.style.display = 'none';
        c2.style.display = 'none';
        c3.style.display = 'none';
        ci1.style.display = 'none';
        ci2.style.display = 'none';
        ci3.style.display = 'none';


    }

});