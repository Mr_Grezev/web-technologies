﻿document.addEventListener('DOMContentLoaded', () => {

    var elem = document.querySelectorAll('button');
    for (var i = 0; i < elem.length; i++) {
        elem[i].onclick = function () {
            var prnt = this.parentNode;
            var shadow = document.getElementById("zandfloat");
            var flupper = document.getElementById("float-upper");

            shadow.style.zIndex = 1;
            flupper.style.zIndex = 2;
            flupper.innerHTML = prnt.children[4].innerHTML;
        }
    }

    var shadow = document.getElementById("zandfloat");
    shadow.onclick = function (){
        var flupper = document.getElementById("float-upper");
        this.style.zIndex = -1;
        flupper.style.zIndex = -1;
        flupper.innerHTML = '';
    }
    
    var flupper = document.getElementById("float-upper");
    var scrhgt = document.documentElement.clientHeight;
    if (scrhgt < flupper.clientHeight + 100) {
        flupper.style.position = "absolute";
        flupper.style.top = "50px";
        flupper.style.transform = "translate(-50%,0)";
        return;
    }

    flupper.style.position = "fixed";
    flupper.style.top = "50%";
    flupper.style.transform = "translate(-50%, -50%)";
});