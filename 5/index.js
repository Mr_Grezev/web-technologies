﻿document.addEventListener("DOMContentLoaded", n);

var flag = false;

function n() {


    var num = document.getElementsByClassName('num');
    var res = document.getElementsByClassName('calc-result-current-value');
    var eql = document.getElementsByClassName('calc-button-equals')[0];
    var sl = document.getElementsByClassName('sl');
    var point = document.getElementById('point');
    var c = document.getElementById('c');
    var pre = document.getElementById('pre');

    for (var i in num) {
        
        num[i].onclick = addnum;
    }
    for (var i in sl) {
        
        sl[i].onclick = addsl;
    }

    eql.onclick = addeql;
    point.onclick = addpoint;
    c.onclick = function () {
        var res = document.getElementsByClassName('calc-result-current-value')[0];
        var previos = document.getElementsByClassName('calc-result-previous-value')[0];

        res.innerHTML = '0';
        previos.innerHTML = '';
    }

    pre.onclick = del;
}



function addnum(e) {
    var res = document.getElementsByClassName('calc-result-current-value')[0];

    if (flag) {
        res.innerHTML = '';
        flag = false;
    }

    

    if (res.innerHTML.length == 1 && res.innerHTML == '0') {
        res.innerHTML = '';
    }
    res.innerHTML += e.currentTarget.innerHTML;

}

function addsl() {
    var res = document.getElementsByClassName('calc-result-current-value')[0];
    var previos = document.getElementsByClassName('calc-result-previous-value')[0];

    if (res.innerText[res.innerHTML.length - 1] == '.') {
        return;
    }

    if (previos.innerHTML != '') {
        addeql();
    }  

    previos.innerHTML = res.innerHTML + this.innerHTML;
    res.innerHTML = 0;
}

function addeql() {
    var res = document.getElementsByClassName('calc-result-current-value')[0];
    var previos = document.getElementsByClassName('calc-result-previous-value')[0];

    eval('var x =' + previos.innerHTML + res.innerHTML + ';');

    res.innerHTML = x;
    previos.innerHTML = '';

    flag = true;
}

function addpoint() {
    var res = document.getElementsByClassName('calc-result-current-value')[0];
    if (flag) {
        flag = false;
    }

    if (res.innerText.includes('.')) {
        return;
    }

    if (res.innerHTML == '') {
        return;
    }
    res.innerHTML += '.';
}

function del() {

    var res = document.getElementsByClassName('calc-result-current-value')[0];

    if (flag) {
        flag = false;
        res.innerHTML = 0;
        return;
    }

    if (res.innerHTML.length == 0) {
        return;
    }
    if (res.innerHTML.length == 1) {
        res.innerHTML = '0';
        return;
    }

    res.innerHTML = res.innerHTML.substring(0, res.innerHTML.length - 1);
    
}